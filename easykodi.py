﻿#!/usr/bin/pyt

import sys
from os.path import isfile
from collections import namedtuple
from ConfigParser import SafeConfigParser

sys.path.append('/storage/.kodi/addons/virtual.rpi-tools/lib')

import RPi.GPIO as GPIO
import xbmc
from xbmcvfs import File, listdir

Config = namedtuple('Config', (
        'skipPin', 
        'shutdownPin', 
        'saveTimer', 
        'videoPath', 
        'saveFileName', 
        'lastSavedSectionName', 
        'lastPlaylistPositionKey', 
        'lastPlaylistLengthKey',
        'lastVideoTitleKey'
    ))

settings = Config(  skipPin=17, 
                    shutdownPin=3, 
                    saveTimer=5000, 
                    videoPath='/storage/videos/', 
                    saveFileName='/storage/.kodi/addons/service.easykodi/saved.ini', 
                    lastSavedSectionName='LastSaved', 
                    lastPlaylistPositionKey='LastPlaylistPosition',
                    lastPlaylistLengthKey='LastPlaylistLength',
                    lastVideoTitleKey="LastVideoTitle"
                )

class LoadError(Exception):
    def __init__(self, value):
        self.value = value 
    def __str__(self):
        return repr(self.value)

class SaveError(Exception):
    def __init__(self, value):
        self.value = value 
    def __str__(self):
        return repr(self.value)


class App:
    
    def __init__(self, settings):
        self.settings = settings 
        self.saveCountdown = settings.saveTimer
        if not self.check_save_file_integrity():
            self.log_notice("Save file integrity not good")
            self.saveConfig = self.init_save_file()
            self.save_save_config()
        self.saveConfig = self.load_save_config()
        if self.check_playlist_changed():
            self.log_notice("playlist changed")
            self.log_notice
            self.saveConfig = self.init_save_file()
            self.save_save_config()
        self.saveConfig = self.load_save_config()
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.settings.skipPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.settings.shutdownPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(self.settings.skipPin, GPIO.FALLING, callback=self.skip_callback, bouncetime=3500)
        GPIO.add_event_detect(self.settings.shutdownPin, GPIO.FALLING, callback=self.shut_down_callback, bouncetime=3500)
        xbmc.log("easykodi initialized", level=xbmc.LOGNOTICE)

    def run(self):
        # type -> None
        offset = self.get_last_playlist_position()
        self.play_video_offset(offset)
        self.set_player_repeat_all()
        while not xbmc.abortRequested:
            xbmc.sleep(5)
            self.periodically_save_player()
        self.tidy()

    def init_save_file(self):
        # type: () -> None
        self.log_notice("Initializing save file")
        f = File(self.settings.saveFileName, 'w+')
        f.write(" ")
        f.close()
        file = self.load_save_config()
        file.add_section(self.settings.lastSavedSectionName)
        file.set(self.settings.lastSavedSectionName, self.settings.lastPlaylistPositionKey, '0')
        file.set(self.settings.lastSavedSectionName, self.settings.lastPlaylistLengthKey, '0')
        return file 

    def check_save_file_integrity(self):
        # type: () -> bool
        try:
            if isfile(self.settings.saveFileName):
                saveFile = self.load_save_config()
                if (
                    saveFile.has_section(self.settings.lastSavedSectionName) and 
                    saveFile.has_option(self.settings.lastSavedSectionName, self.settings.lastPlaylistPositionKey) and 
                    saveFile.has_option(self.settings.lastSavedSectionName, self.settings.lastPlaylistLengthKey)
                ):
                    saveFile.getint(self.settings.lastSavedSectionName, self.settings.lastPlaylistPositionKey)
                    saveFile.getint(self.settings.lastSavedSectionName, self.settings.lastPlaylistLengthKey)
                    return True 
            return False 
        except:
            return False 

    def check_playlist_changed(self):
        # type: () -> bool
        self.get_title_at(self.get_last_playlist_position)
        self.log_notice("last playlist length=" + str(self.get_last_playlist_length()))
        self.log_notice("num video files=" + str(self.get_num_video_files()))
        return self.get_last_playlist_length() != self.get_num_video_files()

    def save_player_now(self):
        # type: () -> None
        playlistLength = self.get_playlist_length()
        playlistPosition = self.get_playlist_position()
        videoTitle = self.get_title_at(playlistPosition)
        self.log_notice("title at " + str(playlistPosition) + "=" + videoTitle)
        if playlistLength > 0 and playlistPosition > 0 and videoTitle:
            self.log_notice("easykodi Video title: " + videoTitle)
            self.set_playlist_position(playlistPosition)
            self.set_playlist_length(playlistLength)
            self.save_save_config()
        else:
            self.log_notice("Nothing to save")
            

    def load_save_config(self):
        # type: () -> SafeConfigParser
        parser = SafeConfigParser()
        parser.read(self.settings.saveFileName)
        return parser

    def save_save_config(self):
        # type: () -> None
        f = File(self.settings.saveFileName, 'w+')
        self.saveConfig.write(f)
        f.close()

    def periodically_save_player(self):
        # type: () -> None
        if(self.saveCountdown > 0):
            self.saveCountdown -= 1
        else:
            self.save_player_now()
            self.saveCountdown = self.settings.saveTimer

    def play_video_offset(self, offset):
        command = "PlayMedia(" + self.settings.videoPath + ",isdir"
        if offset >= 1:
            command += ",playoffset=" + str(offset)
        command += ")"
        xbmc.executebuiltin(command)

    def set_player_repeat_all(self):
        xbmc.executebuiltin("PlayerControl(RepeatAll)")

    def tidy(self):
        # type: () -> None
        self.log_notice("easykodi tidying")
        GPIO.remove_event_detect(self.settings.skipPin)
        GPIO.cleanup([self.settings.skipPin])

    def skip_callback(self, channel):
        # type -> None
        playlistLength = self.get_playlist_length()
        playlistPosition = self.get_playlist_position()
        videoTitle = self.get_title_at(playlistPosition)
        self.log_notice("title at " + str(playlistPosition) + "=" + videoTitle)
        if playlistLength > 0 and playlistPosition > 0 and videoTitle:
            self.log_notice("Skip to next")
            xbmc.executebuiltin("PlayerControl(Next)")

    def shut_down_callback(self, channel):
        # type -> None
        self.log_notice("Powering down")
        xbmc.executebuiltin("Powerdown")

    def integer(self, num):
        if num:
            return int(num)
        else:
            return -1

    def get_playlist_length(self):
        # type: () -> int
        return self.integer(xbmc.getInfoLabel("Playlist.Length(video)"))

    def get_playlist_position(self):
        # type: () -> int
        return self.integer(xbmc.getInfoLabel("Playlist.Position(video)"))

    def get_title_at(self, position):
        # type: (int) -> str
        return str(xbmc.getInfoLabel("VideoPlayer.Position(" + str(position) + ").Title"))

    def get_last_playlist_position(self):
        # type: () -> int
        return self.saveConfig.getint(self.settings.lastSavedSectionName, self.settings.lastPlaylistPositionKey)

    def get_last_playlist_length(self):
        # type: () -> int
        return self.saveConfig.getint(self.settings.lastSavedSectionName, self.settings.lastPlaylistLengthKey)

    def get_num_video_files(self):
        dirs, files = listdir(self.settings.videoPath)
        return len([name for name in files])

    def set_playlist_position(self, position):
        # type: (int) -> None
        self.saveConfig.set(self.settings.lastSavedSectionName, self.settings.lastPlaylistPositionKey, str(position))

    def set_playlist_length(self, length):
        # type: (int) -> None
        self.saveConfig.set(self.settings.lastSavedSectionName, self.settings.lastPlaylistLengthKey, str(length))

    def log_notice(self, message):
        xbmc.log("easykodi: " + message, level=xbmc.LOGNOTICE)    

        
	
if (__name__ == "__main__"):
    xbmc.log("Starting easykodi", level=xbmc.LOGNOTICE)
    main = App(settings)
    main.run()
